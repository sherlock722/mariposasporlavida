#import "_Familias.h"

@interface Familias : _Familias {}

+(id) familyWithName:(NSString *)aName inContext:(NSManagedObjectContext *)aContext;

@end
