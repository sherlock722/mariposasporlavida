//
//  ModelDAO.h
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 02/04/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AGTCoreDataStack.h"
#import "MPVDataFilter.h"

@interface ModelDAO : NSObject

@property (strong, nonatomic) AGTCoreDataStack *dataStack;
@property (strong, nonatomic) MPVDataFilter *dataFilter;

+(ModelDAO *)sharedModelDAO;
+(ModelDAO *)defaultModelDAO;

-(NSFetchedResultsController *) obtainFamilies;
-(NSFetchedResultsController *) obtainSpecies;

@end
