//
//  MPVFamiliesTableVC.m
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 12/06/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import "MPVFamiliesTableVC.h"
#import "MPVFamiliesTableCell.h"
#import "MPVSpeciesTableVC.h"
#import "Familias.h"
#import "ModelDAO.h"

@interface MPVFamiliesTableVC ()

@end

@implementation MPVFamiliesTableVC

-(id) init {
    if (self = [super initWithFetchedResultsController:[[ModelDAO defaultModelDAO] obtainFamilies] style:UITableViewStylePlain]) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.tableView.editing = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView registerClass:[MPVFamiliesTableCell class] forCellReuseIdentifier:@"MPVFamiliesTableCell"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MPVFamiliesTableCell";
    MPVFamiliesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MPVFamiliesTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:cellIdentifier];
    }
    
    Familias *familia = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //Nombre de la familia
    cell.nameLabel.text = familia.nombreF;
    cell.colorLabel.backgroundColor = [UIColor greenColor];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //La Familia seleccionada
    Familias *familia = [self.fetchedResultsController objectAtIndexPath:indexPath];
    [ModelDAO defaultModelDAO].dataFilter.families = [[NSSet alloc] initWithArray:@[familia.nombreF]];
    //Hacemos push al combinador
    [self.navigationController pushViewController:[[MPVSpeciesTableVC alloc] init] animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

@end
