//
//  MPVSpeciesTableCell.h
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 12/06/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPVSpeciesTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *colorLabel;
@property (weak, nonatomic) IBOutlet UIImageView *photoView;

@end
