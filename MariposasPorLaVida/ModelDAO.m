//
//  ModelDAO.m
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 02/04/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import "ModelDAO.h"

@implementation ModelDAO

-(id)init {
    if (self = [super init]) {
        _dataStack = [AGTCoreDataStack coreDataStackWithModelName:@"MariposasModel"];
    }
    return self;
}

+(ModelDAO *)sharedModelDAO{
    static ModelDAO *sharedDAO = nil;
    
    if (!sharedDAO) {
        sharedDAO = [[self alloc]init];
    }
    return sharedDAO;
}

+(ModelDAO *)defaultModelDAO{
    static ModelDAO *defaultDAO = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultDAO = [[self alloc]init];
    });
    return defaultDAO;
}

-(NSFetchedResultsController *) obtainFamilies {
    //Creamos la solicitud al modelo
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Familias"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"nombreF" ascending:NO]];
    
    //Creamos el controlador
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:self.dataStack.context
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    return fetchedResultsController;
}

-(NSFetchedResultsController *) obtainSpecies {
    //Creamos la solicitud al modelo
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Especies"];
    fetchRequest.sortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"nombreE" ascending:NO]];
    
    //Creamos el controlador
    NSFetchedResultsController *fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                                                                               managedObjectContext:self.dataStack.context
                                                                                                 sectionNameKeyPath:nil
                                                                                                          cacheName:nil];
    return fetchedResultsController;
}

@end
