// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Especies.m instead.

#import "_Especies.h"

const struct EspeciesAttributes EspeciesAttributes = {
	.autorFoto = @"autorFoto",
	.gradoExtincion = @"gradoExtincion",
	.imagenE = @"imagenE",
	.nombreE = @"nombreE",
	.thumbnailE = @"thumbnailE",
};

const struct EspeciesRelationships EspeciesRelationships = {
	.ea = @"ea",
	.emv = @"emv",
	.ep = @"ep",
	.familia = @"familia",
};

const struct EspeciesFetchedProperties EspeciesFetchedProperties = {
};

@implementation EspeciesID
@end

@implementation _Especies

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Especies" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Especies";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Especies" inManagedObjectContext:moc_];
}

- (EspeciesID*)objectID {
	return (EspeciesID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"gradoExtincionValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"gradoExtincion"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic autorFoto;






@dynamic gradoExtincion;



- (int16_t)gradoExtincionValue {
	NSNumber *result = [self gradoExtincion];
	return [result shortValue];
}

- (void)setGradoExtincionValue:(int16_t)value_ {
	[self setGradoExtincion:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveGradoExtincionValue {
	NSNumber *result = [self primitiveGradoExtincion];
	return [result shortValue];
}

- (void)setPrimitiveGradoExtincionValue:(int16_t)value_ {
	[self setPrimitiveGradoExtincion:[NSNumber numberWithShort:value_]];
}





@dynamic imagenE;






@dynamic nombreE;






@dynamic thumbnailE;






@dynamic ea;

	

@dynamic emv;

	

@dynamic ep;

	

@dynamic familia;

	






@end
