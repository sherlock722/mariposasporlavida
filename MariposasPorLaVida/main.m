//
//  main.m
//  MariposasPorLaVida
//
//  Created by  on 22/05/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MPVAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MPVAppDelegate class]));
    }
}
