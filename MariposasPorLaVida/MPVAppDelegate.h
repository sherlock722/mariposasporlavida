//
//  MPVAppDelegate.h
//  MariposasPorLaVida
//
//  Created by  on 22/05/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPVAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
