//
//  MPVSpeciesTableCell.m
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 12/06/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import "MPVSpeciesTableCell.h"

@implementation MPVSpeciesTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"MPVSpeciesTableCell" owner:self options:nil];
        if (arrayOfViews.count > 0) {
            if ([[arrayOfViews objectAtIndex:0] isKindOfClass:[self class]]) {
                return [arrayOfViews objectAtIndex:0];
            } else {
                return nil;
            }
        } else {
            return nil;
        }
    }
    return self;
}

@end
