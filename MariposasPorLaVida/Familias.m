#import "Familias.h"


@interface Familias ()

// Private interface goes here.

@end


@implementation Familias

+(id) familyWithName:(NSString *)aName inContext:(NSManagedObjectContext *)aContext {
    Familias *f = [self insertInManagedObjectContext:aContext];
    f.nombreF = aName;
    f.color = @1;
    return f;
}

@end
