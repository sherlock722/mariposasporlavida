//
//  MPVDataFilter.h
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 12/06/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MPVDataFilter : NSObject

@property (strong, nonatomic) NSSet *families;
@property (strong, nonatomic) NSSet *size;
@property (strong, nonatomic) NSSet *flightPeriod;
@property (strong, nonatomic) NSSet *nutrition;
@property (strong, nonatomic) NSSet *provinces;
@property (strong, nonatomic) NSSet *extinctionGrade;

@end
