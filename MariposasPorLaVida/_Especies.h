// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Especies.h instead.

#import <CoreData/CoreData.h>


extern const struct EspeciesAttributes {
	__unsafe_unretained NSString *autorFoto;
	__unsafe_unretained NSString *gradoExtincion;
	__unsafe_unretained NSString *imagenE;
	__unsafe_unretained NSString *nombreE;
	__unsafe_unretained NSString *thumbnailE;
} EspeciesAttributes;

extern const struct EspeciesRelationships {
	__unsafe_unretained NSString *ea;
	__unsafe_unretained NSString *emv;
	__unsafe_unretained NSString *ep;
	__unsafe_unretained NSString *familia;
} EspeciesRelationships;

extern const struct EspeciesFetchedProperties {
} EspeciesFetchedProperties;

@class NSManagedObject;
@class NSManagedObject;
@class NSManagedObject;
@class Familias;



@class NSObject;

@class NSObject;

@interface EspeciesID : NSManagedObjectID {}
@end

@interface _Especies : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (EspeciesID*)objectID;





@property (nonatomic, strong) NSString* autorFoto;



//- (BOOL)validateAutorFoto:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* gradoExtincion;



@property int16_t gradoExtincionValue;
- (int16_t)gradoExtincionValue;
- (void)setGradoExtincionValue:(int16_t)value_;

//- (BOOL)validateGradoExtincion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id imagenE;



//- (BOOL)validateImagenE:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* nombreE;



//- (BOOL)validateNombreE:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id thumbnailE;



//- (BOOL)validateThumbnailE:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSManagedObject *ea;

//- (BOOL)validateEa:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSManagedObject *emv;

//- (BOOL)validateEmv:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) NSManagedObject *ep;

//- (BOOL)validateEp:(id*)value_ error:(NSError**)error_;




@property (nonatomic, strong) Familias *familia;

//- (BOOL)validateFamilia:(id*)value_ error:(NSError**)error_;





@end

@interface _Especies (CoreDataGeneratedAccessors)

@end

@interface _Especies (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAutorFoto;
- (void)setPrimitiveAutorFoto:(NSString*)value;




- (NSNumber*)primitiveGradoExtincion;
- (void)setPrimitiveGradoExtincion:(NSNumber*)value;

- (int16_t)primitiveGradoExtincionValue;
- (void)setPrimitiveGradoExtincionValue:(int16_t)value_;




- (id)primitiveImagenE;
- (void)setPrimitiveImagenE:(id)value;




- (NSString*)primitiveNombreE;
- (void)setPrimitiveNombreE:(NSString*)value;




- (id)primitiveThumbnailE;
- (void)setPrimitiveThumbnailE:(id)value;





- (NSManagedObject*)primitiveEa;
- (void)setPrimitiveEa:(NSManagedObject*)value;



- (NSManagedObject*)primitiveEmv;
- (void)setPrimitiveEmv:(NSManagedObject*)value;



- (NSManagedObject*)primitiveEp;
- (void)setPrimitiveEp:(NSManagedObject*)value;



- (Familias*)primitiveFamilia;
- (void)setPrimitiveFamilia:(Familias*)value;


@end
