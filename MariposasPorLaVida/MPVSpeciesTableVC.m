//
//  MPVSpeciesTableVC.m
//  MariposasPorLaVida
//
//  Created by PABLO GONZALEZ ROCA on 12/06/13.
//  Copyright (c) 2013 PABLO GONZALEZ ROCA. All rights reserved.
//

#import "MPVSpeciesTableVC.h"
#import "MPVSpeciesTableCell.h"
#import "Especies.h"
#import "ModelDAO.h"

@interface MPVSpeciesTableVC ()

@end

@implementation MPVSpeciesTableVC

-(id) init {
    if (self = [super initWithFetchedResultsController:[[ModelDAO defaultModelDAO] obtainSpecies] style:UITableViewStylePlain]) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.tableView.editing = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [self.tableView registerClass:[MPVSpeciesTableCell class] forCellReuseIdentifier:@"MPVSpeciesTableCell"];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableView delegate

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"MPVSpeciesTableCell";
    
    MPVSpeciesTableCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (cell == nil) {
        cell = [[MPVSpeciesTableCell alloc] initWithStyle:UITableViewCellStyleDefault
                                           reuseIdentifier:cellIdentifier];
    }
    
    Especies *especie = [self.fetchedResultsController objectAtIndexPath:indexPath];
    //Nombre de la especie
    cell.textLabel.text = especie.nombreE;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //La Especie seleccionada
    //Especies *especie = [self.fetchedResultsController objectAtIndexPath:indexPath];
    
    //Hacemos push al combinador
    //[self.navigationController pushViewController:[[PGRNotesTabBarVC alloc] initWithNotebook:notebook] animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}

@end
