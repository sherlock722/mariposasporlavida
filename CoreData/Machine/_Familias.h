// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Familias.h instead.

#import <CoreData/CoreData.h>


extern const struct FamiliasAttributes {
	__unsafe_unretained NSString *color;
	__unsafe_unretained NSString *descripcion;
	__unsafe_unretained NSString *imagenF;
	__unsafe_unretained NSString *nombreF;
	__unsafe_unretained NSString *thumbnailF;
} FamiliasAttributes;

extern const struct FamiliasRelationships {
	__unsafe_unretained NSString *mariposas;
} FamiliasRelationships;

extern const struct FamiliasFetchedProperties {
} FamiliasFetchedProperties;

@class Especies;



@class NSObject;

@class NSObject;

@interface FamiliasID : NSManagedObjectID {}
@end

@interface _Familias : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FamiliasID*)objectID;





@property (nonatomic, strong) NSNumber* color;



@property int16_t colorValue;
- (int16_t)colorValue;
- (void)setColorValue:(int16_t)value_;

//- (BOOL)validateColor:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* descripcion;



//- (BOOL)validateDescripcion:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id imagenF;



//- (BOOL)validateImagenF:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* nombreF;



//- (BOOL)validateNombreF:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) id thumbnailF;



//- (BOOL)validateThumbnailF:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSSet *mariposas;

- (NSMutableSet*)mariposasSet;





@end

@interface _Familias (CoreDataGeneratedAccessors)

- (void)addMariposas:(NSSet*)value_;
- (void)removeMariposas:(NSSet*)value_;
- (void)addMariposasObject:(Especies*)value_;
- (void)removeMariposasObject:(Especies*)value_;

@end

@interface _Familias (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveColor;
- (void)setPrimitiveColor:(NSNumber*)value;

- (int16_t)primitiveColorValue;
- (void)setPrimitiveColorValue:(int16_t)value_;




- (NSString*)primitiveDescripcion;
- (void)setPrimitiveDescripcion:(NSString*)value;




- (id)primitiveImagenF;
- (void)setPrimitiveImagenF:(id)value;




- (NSString*)primitiveNombreF;
- (void)setPrimitiveNombreF:(NSString*)value;




- (id)primitiveThumbnailF;
- (void)setPrimitiveThumbnailF:(id)value;





- (NSMutableSet*)primitiveMariposas;
- (void)setPrimitiveMariposas:(NSMutableSet*)value;


@end
