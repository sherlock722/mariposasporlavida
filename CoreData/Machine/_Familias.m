// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Familias.m instead.

#import "_Familias.h"

const struct FamiliasAttributes FamiliasAttributes = {
	.color = @"color",
	.descripcion = @"descripcion",
	.imagenF = @"imagenF",
	.nombreF = @"nombreF",
	.thumbnailF = @"thumbnailF",
};

const struct FamiliasRelationships FamiliasRelationships = {
	.mariposas = @"mariposas",
};

const struct FamiliasFetchedProperties FamiliasFetchedProperties = {
};

@implementation FamiliasID
@end

@implementation _Familias

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Familias" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Familias";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Familias" inManagedObjectContext:moc_];
}

- (FamiliasID*)objectID {
	return (FamiliasID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"colorValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"color"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic color;



- (int16_t)colorValue {
	NSNumber *result = [self color];
	return [result shortValue];
}

- (void)setColorValue:(int16_t)value_ {
	[self setColor:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveColorValue {
	NSNumber *result = [self primitiveColor];
	return [result shortValue];
}

- (void)setPrimitiveColorValue:(int16_t)value_ {
	[self setPrimitiveColor:[NSNumber numberWithShort:value_]];
}





@dynamic descripcion;






@dynamic imagenF;






@dynamic nombreF;






@dynamic thumbnailF;






@dynamic mariposas;

	
- (NSMutableSet*)mariposasSet {
	[self willAccessValueForKey:@"mariposas"];
  
	NSMutableSet *result = (NSMutableSet*)[self mutableSetValueForKey:@"mariposas"];
  
	[self didAccessValueForKey:@"mariposas"];
	return result;
}
	






@end
